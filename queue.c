/**
 * @file queue.c
 * @brief Implementation of a queue that supports FIFO and LIFO operations.
 *
 * This queue implementation uses a singly-linked list to represent the
 * queue elements. Each queue element stores a string value.
 *
 * Assignment for basic C skills diagnostic.
 * Developed for courses 15-213/18-213/15-513 by R. E. Bryant, 2017
 * Extended to store strings, 2018
 *
 * TODO: fill in your name and Andrew ID
 * @author XXX <XXX@andrew.cmu.edu>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "harness.h"
#include "queue.h"

size_t _size_of_str(const char *s) {
  return strlen(s) + 1;
}

list_ele_t *_prev_ele(const queue_t *q, const list_ele_t *e) {
  if (q->size == 1) {
    return NULL;
  }
  list_ele_t *i = q->head;
  for (; i != NULL; i = i->next) {
    if (i->next == e) {
      return i;
    }
  }
  return NULL;
}
/**
 * @brief Allocates a new queue
 * @return The new queue, or NULL if memory allocation failed
 */
queue_t *queue_new(void) {
  queue_t *q = malloc(sizeof(queue_t));
  /* What if malloc returned NULL? */
  if (q == NULL) {
    return NULL;
  }
  q->head = NULL;
  q->tail = NULL;
  q->size = 0;
  return q;
}

/**
 * @brief Frees all memory used by a queue
 * @param[in] q The queue to free
 */
void queue_free(queue_t *q) {
  /* How about freeing the list elements and the strings? */
  /* Free queue structure */
  while (queue_remove_head(q, NULL, 0));
  free(q);
}

/**
 * @brief Attempts to insert an element at head of a queue
 *
 * This function explicitly allocates space to create a copy of `s`.
 * The inserted element points to a copy of `s`, instead of `s` itself.
 *
 * @param[in] q The queue to insert into
 * @param[in] s String to be copied and inserted into the queue
 *
 * @return true if insertion was successful
 * @return false if q is NULL, or memory allocation failed
 */
bool queue_insert_head(queue_t *q, const char *s) {
  list_ele_t *newh;
  /* What should you do if the q is NULL? */
  if (q == NULL) {
    return false;
  }
  newh = malloc(sizeof(list_ele_t));
  newh->value = malloc(sizeof(char) * _size_of_str(s));
  if (newh == NULL || newh->value == NULL) {
    return false;
  }
  strcpy(newh->value, s);
  /* Don't forget to allocate space for the string and copy it */
  /* What if either call to malloc returns NULL? */
  newh->next = q->head;
  q->head = newh;
  q->tail = (q->size == 0) ? newh : q->tail;
  q->size += 1;
  return true;
}

/**
 * @brief Attempts to insert an element at tail of a queue
 *
 * This function explicitly allocates space to create a copy of `s`.
 * The inserted element points to a copy of `s`, instead of `s` itself.
 *
 * @param[in] q The queue to insert into
 * @param[in] s String to be copied and inserted into the queue
 *
 * @return true if insertion was successful
 * @return false if q is NULL, or memory allocation failed
 */
bool queue_insert_tail(queue_t *q, const char *s) {
  /* You need to write the complete code for this function */
  /* Remember: It should operate in O(1) time */
  if (q == NULL) {
    return NULL;
  }

  list_ele_t *newt = malloc(sizeof(list_ele_t));
  if (newt == NULL) {
    return NULL;
  }
  newt->value = malloc(sizeof(char) * _size_of_str(s));
  strcpy(newt->value, s);
  newt->next = NULL;

  if (q->size == 0) {
    q->head = newt;
  } else {
    q->tail->next = newt;
  }
  q->tail = newt;
  q->size += 1;
  return true;
}

/**
 * @brief Attempts to remove an element from head of a queue
 *
 * If removal succeeds, this function frees all memory used by the
 * removed list element and its string value before returning.
 *
 * If removal succeeds and `buf` is non-NULL, this function copies up to
 * `bufsize - 1` characters from the removed string into `buf`, and writes
 * a null terminator '\0' after the copied string.
 *
 * @param[in]  q       The queue to remove from
 * @param[out] buf     Output buffer to write a string value into
 * @param[in]  bufsize Size of the buffer `buf` points to
 *
 * @return true if removal succeeded
 * @return false if q is NULL or empty
 */
bool queue_remove_head(queue_t *q, char *buf, size_t bufsize) {
  /* You need to fix up this code. */
  if (q == NULL || q->size == 0) {
    return false;
  }

  list_ele_t *ele = q->head;
  q->head = q->head->next;
  if (q->size == 1) {
    q->tail = NULL;
  }
  q->size -= 1;

  if (buf != NULL && bufsize > 0) {
    size_t size_of_str = _size_of_str(ele->value);
    size_t size = (bufsize <= size_of_str) ? bufsize-1 : size_of_str-1;
    memcpy(buf, ele->value, size);
    buf[size] = '\0';
  }

  free(ele->value);
  free(ele);
  return true;
}

/**
 * @brief Returns the number of elements in a queue
 *
 * This function runs in O(1) time.
 *
 * @param[in] q The queue to examine
 *
 * @return the number of elements in the queue, or
 *         0 if q is NULL or empty
 */
size_t queue_size(queue_t *q) {
  /* You need to write the code for this function */
  /* Remember: It should operate in O(1) time */
  size_t size = q == NULL ? 0 : q->size;
  return size;
}


/**
 * @brief Reverse the elements in a queue
 *
 * This function does not allocate or free any list elements, i.e. it does
 * not call malloc or free, including inside helper functions. Instead, it
 * rearranges the existing elements of the queue.
 *
 * @param[in] q The queue to reverse
 */
void queue_reverse(queue_t *q) {
  /* You need to write the code for this function */
  if (q == NULL) {
    return;
  }
  switch(q->size) {
    case 0:
    case 1: return;
  }
  list_ele_t *l = q->head;
  list_ele_t *r = q->tail;
  list_ele_t *l_next = l->next;
  l->next = r->next;
  r->next = l_next;
  q->tail = q->head;
  q->head = r;

  /*
  size_t i = 1;
  size_t threshold = q->size / 2;
  for (; i < threshold; ++i, r = _prev_ele(q, l), l = r->next) {
    _prev_ele(q, l)->next = r;
    _prev_ele(q, r)->next = l;
    r_next = r->next;
    r->next = l->next;
    l->next = r_next;
  }
  */
}
